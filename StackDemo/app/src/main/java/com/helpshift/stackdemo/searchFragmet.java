package com.helpshift.stackdemo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

public class searchFragmet extends Fragment {

    // URL to get stack exhange data.
    private static String url = "https://api.stackexchange.com/2.2/questions?order=desc&sort=activity&site=stackoverflow";
    private static String searchUrlPrefix = "https://api.stackexchange.com/2.2/search?order=desc&sort=activity&intitle=";
    private static String searchUrlSuffix = "&site=stackoverflow";

    public Activity mActivity;
    ListView mListview;
    QuestionAdapter mAdapter;
    ListItems mListItems[];
    GetStackData st;
    private SQLiteDatabase db;
    private TextView mTextView;

    private static final String TABLE_NAME = "cache";
    private static final String COLUMN_QUERY = "query";
    private static final String FILE_PATH = "path";

    private FileOutputStream fOut;
    File cacheDir;
    Cursor mCursor;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        mActivity = getActivity();
        View rootView = inflater.inflate(R.layout.main_fragment, container, false);

        //get SQLiteDB_stackDB
        StackDB mDbHelper = new StackDB(mActivity);
        db = mDbHelper.getWritableDatabase();

        cacheDir = new File(mActivity.getCacheDir().getPath());

        mAdapter = new QuestionAdapter(getActivity());

        mTextView = (TextView) rootView.findViewById(R.id.noresult);
        SearchView searchView = (SearchView) rootView.findViewById(R.id.search_view);
        searchView.onActionViewExpanded();
        searchView.setOnQueryTextListener(mOnQueryTextListener);

        mListview = (ListView) rootView.findViewById(R.id.listview);
        mListview.setOnItemClickListener(mOnItemClickListener);

        return rootView;
    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (isNetworkAvailable(mActivity)) {            // default top 20 qouestions.
            st = new GetStackData("default");
            st.execute(url);
        } else {                                        // Offline
            offlineCaching("default");
        }
    }

    class GetStackData extends AsyncTask<String, Void, ListItems[]> {

        private ProgressDialog progressDialog;
        private String query;
        private long newRowId = 0;
        private int count = 0;

        public GetStackData(String query) {
            this.query = query;
        }

        protected void onPreExecute() {

            progressDialog = new ProgressDialog(mActivity);
            progressDialog.setMessage("Loading...");
            progressDialog.show();
        }

        @Override
        protected ListItems[] doInBackground(String... params) {
            // TODO Auto-generated method stub
            HttpService stackService = new HttpService();
            String result;
            // making a request to server for Json
            result = stackService.makeServiceCall(params[0], 1);

            ParseJson parseJson = new ParseJson();
            mListItems = parseJson.getJsonObject(result);

            if (mListItems.length > 0) {
                //file handling for saving search result or deafultQuery
                String fPath = saveResult(query, result);

                ContentValues values = new ContentValues();
                values.put(COLUMN_QUERY, query);
                values.put(FILE_PATH, fPath);

                //Replcae the path of DB if already stored same Query
                Cursor res = db.rawQuery("select " + COLUMN_QUERY + " from cache", null);
                while (count < res.getCount()) {
                    res.moveToNext();
                    String data = res.getString(res.getColumnIndex(COLUMN_QUERY));
                    if (data.equals(query)) {
                        String where = "query = ?";
                        String arg[] = {query};
                        newRowId = db.update(TABLE_NAME, values, where, arg);
                        break;
                    }
                    count++;
                }
                if (newRowId == 0) { // there is no existing data present in stackDB
                    newRowId = db.insert(TABLE_NAME, null, values);
                }
            }

            return mListItems;
        }


        @Override
        protected void onPostExecute(ListItems[] listItemes) {
            super.onPostExecute(listItemes);
            if (listItemes.length > 0) {
                mListview.setVisibility(View.VISIBLE);
                mTextView.setVisibility(View.GONE);
                mAdapter.set(mListItems);
                mListview.setAdapter(mAdapter);
            } else {
                mListview.setVisibility(View.GONE);
                mTextView.setVisibility(View.VISIBLE);
            }
            progressDialog.dismiss();
        }
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    private AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //Not Implemented yet
            ItemDetailFragment fragment = new ItemDetailFragment();
            FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.searchFragment, fragment);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
        }
    };

    private SearchView.OnQueryTextListener mOnQueryTextListener = new SearchView.OnQueryTextListener() {
        @Override
        public boolean onQueryTextSubmit(String query) {
            if (query.contains(" ")) {
                query = query.replace(" ", "%20");
            }
            if (isNetworkAvailable(mActivity)) {
                String searchQuery = searchUrlPrefix + query + searchUrlSuffix;
                GetStackData getStackData = new GetStackData(query);
                getStackData.execute(searchQuery);
            } else {
                offlineCaching(query);
            }
            return true;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            return false;
        }
    };

    private void offlineCaching(String query) {
        int count = 0;
        mCursor = db.rawQuery("select * from cache", null);
        while (count < mCursor.getCount()) {
            mCursor.moveToNext();
            String data = mCursor.getString(mCursor.getColumnIndex(COLUMN_QUERY));
            if (data.equals(query)) {
                String resultJson = mCursor.getString(mCursor.getColumnIndex(FILE_PATH));
                // open file and Read
                StringBuilder sstringBuilderr = new StringBuilder();
                try {
                    FileInputStream fileInputStream = new FileInputStream(new File(resultJson));
                    BufferedReader reader = new BufferedReader(new InputStreamReader(fileInputStream));
                    sstringBuilderr.append(reader.readLine());

                } catch (Exception e) {
                    Log.d("Caught", e + "");
                }
                ParseJson parseJson = new ParseJson();
                mListItems = parseJson.getJsonObject(sstringBuilderr.toString());
                mListview.setVisibility(View.VISIBLE);
                mTextView.setVisibility(View.GONE);
                mAdapter.set(mListItems);
                mListview.setAdapter(mAdapter);
                break;
            }
            count++;
        }
        if (count == mCursor.getCount()) {
            mListview.setVisibility(View.GONE);
            mTextView.setVisibility(View.VISIBLE);
        }
    }

    private String saveResult(String query, String result) {
        File cacheFile = new File(cacheDir, query + ".txt");

        try {
            if (cacheFile.exists()) {
                cacheFile.delete();
            }
            cacheFile.createNewFile();
            fOut = new FileOutputStream(cacheFile);
            fOut.write(result.getBytes());
            fOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return cacheFile.getAbsolutePath();
    }
}

