package com.helpshift.stackdemo;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Hp on 14-07-2015.
 */
public class ParseJson {

    ListItems listItems[];
    JSONObject rootObject;
    JSONArray jsonArray;
    JSONArray tags;
    JSONObject isAnswered;
    JSONObject owner;
    JSONObject ownerDisplayName;
    JSONObject viewCount;
    JSONObject answerCount;
    JSONObject questionId;
    JSONObject link;
    String title;

    public ListItems[] getJsonObject(String jsonString) {
        try {
            rootObject = new JSONObject(jsonString);
            jsonArray = rootObject.optJSONArray("items");
            int length = jsonArray.length();

            if (length > 20)
                length = 20;
            listItems = new ListItems[length];
            for (int i = 0; i < length; i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                tags = jsonObject.optJSONArray("tags");
                isAnswered = jsonObject.optJSONObject("is_answered");
                link = jsonObject.optJSONObject("link");
                title = jsonObject.optString("title").toString();

                listItems[i] = new ListItems();
                listItems[i].title = title;

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listItems;
    }
}
