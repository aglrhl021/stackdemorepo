package com.helpshift.stackdemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;

public class AllInOne extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_entry);

		searchFragmet searchFrag = (searchFragmet)
                getFragmentManager().findFragmentById(R.id.searchFragment);
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.all_in_one, menu);
		return true;
	}
}
