package com.helpshift.stackdemo;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class QuestionAdapter extends BaseAdapter{
	
	LayoutInflater inflater;
    TextView textView;
    ListItems[] listItems;
    ViewHolder holder;

    public QuestionAdapter(Activity activity) {
		// TODO Auto-generated constructor stub
		inflater = LayoutInflater.from(activity);
	}

   public void set(ListItems[] listItem) {
       this.listItems = listItem;
   }

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listItems.length;
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return listItems[arg0];
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(int position, View convertview, ViewGroup parent) {
		// TODO Auto-generated method stub
		if(convertview == null) {
			convertview = inflater.inflate(R.layout.list_item, null);
			holder = new ViewHolder();
			holder.title = (TextView)convertview.findViewById(R.id.textview);
			holder.title.setText(listItems[position].title);
			convertview.setTag(holder);
		}else {
			holder = (ViewHolder)convertview.getTag();
		}
		return convertview;
	}

}

class ViewHolder {
        TextView title;
        }

class ListItems {
	String tags[];
    Owner owner;
    boolean isAnswered;
    int viewcount;
    int answerCount;
    Long questionId;
    String link;
    String title;
	
}

class Owner{
    String displayName;
}
